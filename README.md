# Erlang cluster connectivity and transitivity check

## The idea

This python script is to check if all the erlang cluster nodes are connected and visible to each other. It executes simple ```nodes()``` command and compares the connection list with all the pods found.

If there are connection missing it returns dictionary for every related pod.

## Comments

All the code specific commentary provided in script itself.

To execute script edit the **cluster** variable filling only contexts for ***one kazoo cluster***. For example: there are 3 k8s clusters for one cirrus kazoo cluster. That means you should add all three contexts to **clusters** variable.

## Execution

```python
python script_kazoo_func.py
```
