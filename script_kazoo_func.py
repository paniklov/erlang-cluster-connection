import subprocess
import time
import re


"""NOTE: Because of cirrus example i made a decision to summarize pods with multiple clusters"""

# Get contexts from KUBECONFIG env. Possibly wrong idea as mentioned above.
#config = subprocess.Popen(["kubectl", "config", "get-contexts"], 
#        stdout=subprocess.PIPE, stderr=None, text=True)
#names = subprocess.Popen(["awk", "NR!=1 {print $2}"], 
#        stdin=config.stdout, stderr=None, text=True, stdout=subprocess.PIPE)
#clusters = str(names.communicate()[0]).splitlines()

# Specify label to filter pods
LABEL_ARG = "--selector=kazooService=kazoo-apps"

# Specify contexts manually
#clusters = ['kubernetes-admin-kazoo5-k8s-cirrus3-melbourne@kazoo5-k8s-cirrus3-melbourne', 'kubernetes-admin-minimus-paniklov@minimus-paniklov']
clusters = ['kubernetes-admin-kazoo5-k8s-delphi1-osaka@kazoo5-k8s-delphi1-osaka', 'kubernetes-admin-kazoo5-k8s-delphi2-tokyo@kazoo5-k8s-delphi2-tokyo', 'kubernetes-admin-kazoo5-k8s-delphi3-seoul@kazoo5-k8s-delphi3-seoul']
#clusters = ['kubernetes-admin-minimus@minimus']

# Declare empty dicts
all_pods = []
pod_dict = {}
error_dict = {}

def find_pods(clusters: list):

    """Function will return generator with NAME, NAMESPACE and IP values for found pods.
    After generator reaches end it prints total pods found over all clusters"""

    #total_pods = 0
    for cluster in clusters:

        # Empty list for comparing in the end
        subprocess.run(["kubectl", "config", "use-context", cluster])
    
        # get pods ns, name and ip with kazoo-apps label
        first_command = subprocess.Popen(
            [
                "kubectl", "get", "pods", 
                "--no-headers", 
                LABEL_ARG,
                "--all-namespaces", 
                "-o", "custom-columns=NAMESPACE:.metadata.namespace,NAME:.metadata.name,IP:.status.podIP"
            ], 
            stdout=subprocess.PIPE, text=True
        )
        # Split terminal output by spaces for every line
        output = first_command.communicate()[0].splitlines()

        # Calling exec_nodes function
        exec_nodes(output)

        # Making simple generator
        yield output

    # Comparing pod connections with list of all pods
    for key in pod_dict.keys():
        expr = list(set(all_pods) - set(pod_dict[key]) - set(key))
        if expr:
            error_dict[key] = expr
            #print(f'{key} pod missing connections to {expr}\n')
    print(f'Total pods amount: {len(all_pods)}', '\n')
    if error_dict:
        print(f'Missing connection dictionary: {error_dict}')
    else:
      print(f'No missing connections found', '\n')

def exec_nodes(pods):

    """Function gets pods name, ns and ip as list. Then it executes command for erlang cluster. 
    The results are simple lists with connections themselves and amount for every pod"""


    cluster_output = []


    # Execute erlang nodes() against every pod
    for pod in pods:
        args = pod.split()
        all_pods.append(args[2])
        execute = subprocess.Popen(
            [
                "kubectl", "-n", args[0], 
                "exec", "-ti", args[1], 
                "--", "sup", "erlang", "nodes"
            ], 
            stdout=subprocess.PIPE, text=True
        )
        result = execute.communicate()[0]
        
        # Clearing the string and making list by comma separator
        result = re.sub("[^0-9,-]", "", result)
        result = re.sub("[-]", ".", result).split(",")

        # Filling a dictionary with pod connections
        # Taking dict keys as ip and not the name because erlang command returns ip
        pod_dict[args[2]] = result

        # List with connection number for every erlang node
        cluster_output.append(f'{args[1]}: {len(result)}')

        print(f'Erlang cluster connections for pod {args[2]}:', result, "\n")

        # Sleep for 1 sec just not to load pods too much
        time.sleep(1)
    print(f'Erlang connections amount per pod:', cluster_output, "\n")


my_gen = find_pods(clusters)

# Making gen instance and iterate until it is empty
while True:
    try:
        next(my_gen)
    except StopIteration:
        pass
        break

